import time
import paho.mqtt.client as paho




def publish(message):
    topic = "office/room1"
    #broker="broker.hivemq.com"
    broker="iot.eclipse.org"
    #define callback
    client= paho.Client("client-001") #create client object client1.on_publish = on_publish #assign function to callback client1.connect(broker,port) #establish connection client1.publish("house/bulb1","on")
    ######Bind function to callback
    #####
    print("connecting to broker ",broker)
    client.connect(broker)#connect
    # client.loop_start() #start loop to process received messages
    print("publishing ")
    client.publish(topic, message)#publish
    client.disconnect()

