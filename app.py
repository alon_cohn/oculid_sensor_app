from flask import Flask, jsonify, request, send_from_directory, render_template, redirect
from flask_cors import CORS
from flask_socketio import SocketIO, join_room, emit
from werkzeug.utils import secure_filename
import uuid
import json
import os, datetime
from pub import publish

# configuration
DEBUG = True

# instantiate the app
app = Flask(__name__)

socketio = SocketIO(app, async_mode='threading')

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER = os.path.join(APP_ROOT, 'uploads')
DOWNLOAD_FOLDER = os.path.join(APP_ROOT, 'downloads')

try:
    os.mkdir(UPLOAD_FOLDER)
except:
    print("Cannot create a file when that file already exists")

try:
    os.mkdir(DOWNLOAD_FOLDER)
except:
    print("Cannot create a file when that file already exists")

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config.from_object(__name__)


CORS(app)


# Custom MQTT message callback
def customCallback(client, userdata, message):
    print("Received a new message")
    print(message.payload)
    message_object = message.payload.decode("utf-8")
    print(message_object)
    message_string = json.dumps(message_object)
    print(message_string)
    message_object = json.loads(message_string)
    print(message_object)
    socketio.emit('mqtt_recieve', message_string, broadcast=True)


@socketio.on('mqtt_send')
def on_publish_message(data):
    print(data)
    str_bytes = json.dumps({"message": data}, ensure_ascii=False)
    print(str_bytes)


ROOMS = {}

BOOKS = [
    {
        'id': uuid.uuid4().hex,
        'title': 'On the Road',
        'author': 'Jack Kerouac',
        'read': True
    },
    {
        'id': uuid.uuid4().hex,
        'title': 'Harry Potter and the Philosopher\'s Stone',
        'author': 'J. K. Rowling',
        'read': False
    },
    {
        'id': uuid.uuid4().hex,
        'title': 'Green Eggs and Ham',
        'author': 'Dr. seuss',
        'read': True
    }
]

@app.route('/', methods=['GET'])
def index():
    return render_template("index.html")

@app.route('/', methods=['POST'])
def my_form_post():
    deg = 22
    processed_text = request.form['text']
    try:
        deg = int(processed_text)
        print(deg)
    except:
        return render_template("WrongInput.html")
    if deg<0 or deg>30:
        print("out of range")
        return render_template("WrongInput.html")
    publish(processed_text)
    return redirect("grafana")

@app.route('/grafana',)
def grafana():
    return redirect("http://192.168.1.104:3000/d/7W4mR_Zgk/oculid_temperature_system?orgId=1")

# sanity check route
@app.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify('pong!')



if __name__ == '__main__':
    # app.run(debug=True)
    #socketio.run(app, port=80, debug=True)
    app.run(host='0.0.0.0')